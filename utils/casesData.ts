import { getSortedCasesData } from "../lib/cases";

export async function getStaticProps(){
    const allCasesData= getSortedCasesData()
    return {
        props: {
            allCasesData
        }
    }
}
