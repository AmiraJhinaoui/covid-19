import Head from 'next/head';
import {Layout} from '../components/Layout';
import {ChoiceCard} from '../components/ChoiceCard';
import Link from 'next/link';
import { url } from 'inspector';
import useSWR from 'swr';

const getData = async () =>{
  const res = await fetch ("https://api.covid19api.com/summary");
  return await res.json ();
  
}
 
export default function Home( ) {
  const { data,error} = useSWR("url",getData)
  console.log(data);
  if (error) return <h1>Something went wrong dude</h1>;
  // if (!data) return <Layout> <div className="flex items-center justify-center h-screen text-gray-300" > Loading ... </div></Layout>
 
  return (
    <Layout home>
      
    
        <h5 >Covid 19 </h5>
        <link rel="icon" href="/favicon.ico" />


  
        <h1 className="title ">
         Covid-19 <a href=""></a>
        </h1>

        <p className="description text-red-200">
          <Link href='/hey'>Keep yourself updated  </Link>
        </p>

        <div className="grid">
        <div className="p-5 flex justify-content-center justify-center">
          <ChoiceCard  continentImage= "/images/elephant-facing-right.png"
            cardColor= "red"
            continentName ="Africa"
            newCases={501}
            deaths={601}
          />
          <ChoiceCard  continentImage= "/images/euro-currency-symbol.png"
            cardColor= "red"
            continentName ="Europe"
            newCases={4000}
            deaths={500}
          />
          <ChoiceCard  continentImage= "/images/indian.png"
            cardColor= "red"
            continentName ="America"
            newCases={6500}
            deaths={1000}
          />
          <ChoiceCard  continentImage= "/images/thatbyinnyu-temple.png"
            cardColor= "red"
            continentName ="Asia"
            newCases={1136}
            deaths={842}
          />

          </div>
        </div>


    
     
    </Layout>
  )
}
