import { Layout } from "../components/Layout";
import useSWR from "swr";
import ReactMapGL from 'react-map-gl';
import { useState } from "react";
import { Marker } from "mapbox-gl";
import mapboxgl from "mapbox-gl";



 
export default function Map (){
    const [viewport, setViewport] = useState({
        width: '100vw',
        height: '100vh',
        latitude: 36.51,
        longitude: 10.15,
        zoom: 5
      });
      const getData = async () =>{
        const res = await fetch ("https://api.covid19api.com/countries");
        console.log(res.json ())
        return await res.json ();
    
    }

      //Load data from Api:
      const { data,error} = useSWR('https://api.covid19api.com/countries',getData)
      console.log(data);

      //Add the marker

    return(
        <Layout>
        <ReactMapGL
        mapStyle="mapbox://styles/mapbox/dark-v9"
        mapboxApiAccessToken="pk.eyJ1IjoiYW1pcmFqaCIsImEiOiJja2I5aTd4YmswZWhiMnJwaXM3bHU4ZnYwIn0.-8zxOJkvGAsRGaRTBy8mlg"
        {...viewport}
        onViewportChange={(nextViewport:any) => setViewport(nextViewport)}
      > 
    
      
      </ReactMapGL>
        </Layout>
      
    )
}