import {Layout} from '../components/Layout';
import Map from '../components/map'

export default function CasesUpdate (){
    return(
        <Layout> 
            <Map/>
        </Layout>
    );
}