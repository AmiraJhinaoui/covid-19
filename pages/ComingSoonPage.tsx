import { Layout } from "../components/Layout";
import Head from 'next/head';
export default function ComingSoonPage(){
    return(
        <Layout>
    <div className="p-16 flex justify-content-center justify-center">
            <Head>
                <title >Coming Soon</title>
            </Head>


            <main className="p-16">
                    <h1 className="text-4xl text-center font-bold text-red-700 mb-4 ">
                    COMING SOON
                    </h1>
                    <p className=" p-4 text-lg  text-center text-red-200 mb-16">
                    10 Days left
                    </p> 
                   
                    <div className=" w-64  text-center rounded-t lg:rounded-t-none lg:rounded-l text-center ">
                    <div className="block rounded-t overflow-hidden  text-center ">
                        <div className="bg-red-700 text-white py-1">
                       June
                        </div>
                        <div className="pt-1 border-l border-r border-white bg-red-200">
                            <span className="text-5xl font-bold leading-tight">
                                21
                            </span>
                        </div>
                        <div className="border-l border-r border-b rounded-b-lg text-center border-white bg-red-200 -pt-2 -mb-1">
                            <span className="text-sm">
                                Sunday
                            </span>
                        </div>
                        <div className="pb-2 border-l border-r border-b rounded-b-lg text-center border-white bg-red-200">
                            <span className="text-xs leading-normal">
                            2020
                            </span>
                        </div>
                    </div>
                    </div>
     
            </main>
    </div>
        </Layout>
    )
}