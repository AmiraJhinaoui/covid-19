
import {Layout} from '../components/Layout'
import Head from 'next/head';
import PersonalInfosForm from '../components/covidTestForm/PersonalinfosForm';
import CovidTestQuizDistance from '../components/covidTestForm/CovidTestDistance';
import CovidTestSymptoms from '../components/covidTestForm/CovidTestSymptoms';
import CovidTestSevere from '../components/covidTestForm/CovidTestSevere';


export default function CovidTestPage(){
    return(
        <Layout>
          <div className="p-10 flex justify-content-center justify-center">
      <Head>
        <title >Test Covid</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>


      <main>
        <h1 className="text-4xl text-center font-bold text-red-700 mb-4 ">
         Do you feel sick? 
        </h1>
        <p className="text-lg  text-center text-red-200 mb-6">
           Check yourself and protect everyone around  
        </p>
        <div className="container bg-black bg-opacity-25 ">
       <PersonalInfosForm/>
       {/* <CovidTestQuizDistance/> */}
       {/* <CovidTestSymptoms/> */}
       {/* <CovidTestSevere/> */}
       
       </div>
       </main>
        </div>
    
        </Layout>

        
    )
    
}