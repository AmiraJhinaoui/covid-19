
import { Layout } from "../components/Layout";
import useSWR from 'swr';
import moment from 'moment';

const getData = async () =>{
    const res = await fetch ("https://api.covid19api.com/summary");
    return await res.json ();
    
  }

export default function Sumary(){
    const { data,error} = useSWR('https://api.covid19api.com/summary',getData)
    console.log(data);

    
    if (error) return <h1>Something went wrong dude</h1>;
    if (!data) return <Layout><div className="flex items-center justify-center h-screen text-gray-300" > Loading ... </div></Layout>
    return(

        <Layout>
        <div className="container w-full mx-auto pt-20">
		
		<div className="w-full px-4 md:px-0 md:mt-8 mb-16 text-grey-darkest leading-normal">
      
            <div className="flex justify-center ">
        <h2 className="text-3xl text-red-600 mb-10 mx-3  ">
     {moment(data.Date).format('DD MMMM YYYY ')}
        </h2>
        <h2 className="text-3xl text-gray-300 mb-10 mx-3 ">
     {moment(data.Date).format('HH:mm')}
        </h2>
            </div>
            

<div className="flex flex-wrap">


                <div className="w-full md:w-1/2 xl:w-1/3 p-3">
                  
               {/* New Cases */}
                    <div className="bg-red-500 bg-opacity-25  rounded shadow p-2">
                        <div className="flex flex-row items-center">
                            <div className="flex-shrink pr-4">
                                <div className="rounded p-3 bg-green-dark"><i className="fa fa-wallet fa-2x fa-fw fa-inverse"></i></div>
                            </div>
                            <div><img src="/images/cases.png" /></div>
                            <div className="flex-1 text-right md:text-center">
                                <h5 className="uppercase  text-gray-300">New Cases</h5>
                                <h3 className="text-3xl text-gray-100">{(data.Global.NewConfirmed).toLocaleString()} </h3>
                            </div>
                        </div>
                    </div>
                    
                </div>
                     {/* New Deaths */}
                <div className="w-full md:w-1/2 xl:w-1/3 p-3">
                   
                    <div className="bg-black bg-opacity-25  rounded shadow p-2">
                        <div className="flex flex-row items-center">
                            <div className="flex-shrink pr-4">
                                <div className="rounded p-3 bg-orange-dark"><i className="fas fa-users fa-2x fa-fw fa-inverse"></i></div>
                            </div>
                            <div><img src="/images/death.png"/></div>
                            <div className="flex-1 text-right md:text-center">
                                <h5 className="uppercase text-gray-300">New Deaths</h5>
                                <h3 className="text-3xl text-gray-100">{(data.Global.NewDeaths).toLocaleString()} </h3>
                            </div>
                        </div>
                    </div>
                    
                </div>
                     {/* New Recovered */}
                <div className="w-full md:w-1/2 xl:w-1/3 p-3">
                    
                    <div className="bg-green-500 bg-opacity-25  rounded shadow p-2">
                        <div className="flex flex-row items-center">
                            <div className="flex-shrink pr-4">
                                <div className="rounded p-3 bg-yellow-dark"><i className="fas fa-user-plus fa-2x fa-fw fa-inverse"></i></div>
                            </div>
                            <div> <img src="/images/recovered.png"/></div>
                            <div className="flex-1 text-right md:text-center">
                                <h5 className="uppercase  text-gray-300">New Recovered</h5>
                                <h3 className="text-3xl text-gray-100">{(data.Global.NewRecovered).toLocaleString()} </h3>
                            </div>
                        </div>
                    </div>
                    
                </div>

                     {/* TOTAL Cases */}
                <div className="w-full md:w-1/2 xl:w-1/3 p-3">
         
                    <div className="bg-red-500 bg-opacity-50  rounded shadow p-2">
                        <div className="flex flex-row items-center">
                            <div className="flex-shrink pr-4">
                                <div className="rounded p-3 bg-blue-dark"><i className="fas fa-server fa-2x fa-fw fa-inverse"></i></div>
                            </div>
                            <img src="/images/totalCases.png" />
                            <div className="flex-1 text-right md:text-center">
                                <h5 className="uppercase  text-gray-300">Total Cases</h5>
                                <h3 className="text-3xl text-gray-100">{(data.Global.TotalConfirmed).toLocaleString()}</h3>
                            </div>
                        </div>
                    </div>
                    
                </div>

                     {/* TOTAL Deaths */}
                <div className="w-full md:w-1/2 xl:w-1/3 p-3">
       
                    <div className="bg-black bg-opacity-50  rounded shadow p-2">
                        <div className="flex flex-row items-center">
                            <div className="flex-shrink pr-4">
                                <div className="rounded p-3 bg-indigo-dark"><i className="fas fa-tasks fa-2x fa-fw fa-inverse"></i></div>
                            </div>
                            <img src="/images/totalDeaths.png" />
                            <div className="flex-1 text-right md:text-center">
                                <h5 className="uppercase  text-gray-300">Total Deaths</h5>
                                <h3 className="text-3xl text-gray-100">{(data.Global.TotalDeaths).toLocaleString()}</h3>
                            </div>
                        </div>
                    </div>
                 
                </div>
                     {/* TOTAL Recovered */}
                <div className="w-full md:w-1/2 xl:w-1/3 p-3">
            
                    <div className="bg-green-500 bg-opacity-50  rounded shadow p-2">
                        <div className="flex flex-row items-center">
                            <div className="flex-shrink pr-4">
                                <div className="rounded p-3 bg-red-dark"><i className="fas fa-inbox fa-2x fa-fw fa-inverse"></i></div>
                            </div>
                            <img src="/images/happy.png" />
                            <div className="flex-1 text-right md:text-center">
                                <h5 className="uppercase text-gray-300">Total Recovered</h5>
                                <h3 className="text-3xl text-gray-100">{(data.Global.TotalRecovered).toLocaleString()}</h3>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            </div>
            </div>

        </Layout>
        
    )
}