import useSWR from "swr";
import { Layout } from "./Layout";
import { loadGetInitialProps } from "next/dist/next-server/lib/utils";

export interface ICardProps{
  continentImage:string;
  cardColor: string;
  continentName:string;
  newCases: number;
  deaths: number;
}
const getData = async () =>{
  const res = await fetch ("https://corona.lmao.ninja/v2/continents?yesterday=true&sort");
  return await res.json ();
  
}
 const getInitialProps= async (props: ICardProps)=>{
  console.log("heeehi");
  console.log({...props});
  return {...props};
}
export  function ChoiceCard(cardProps:ICardProps){
  const { data,error} = useSWR('https://corona.lmao.ninja/v2/continents?yesterday=true&sort',getData)
    if (error) return <h1>Something went wrong dude</h1>;
    if (!data) return <Layout><div className="flex items-center justify-center h-screen text-gray-300" > Loading ... </div></Layout>
    return(
    
    <div className="flex-shrink-0 m-6 relative overflow-hidden bg-red-200 bg-opacity-25 rounded-lg max-w-xs shadow-lg">
      <div className=" pt-10 px-10 flex items-center justify-center">
    <div className="block absolute w-48 h-48 bottom-0 left-0 -mb-24 ml-3 " ></div>
        <img className="relative w-40" src={cardProps.continentImage} alt=""/>
      </div>

      <div className="relative text-white px-6 pb-6 mt-6">
        <span className="block opacity-75 -mb-1">{cardProps.continentName}</span>
        <div className="flex justify-between">
          <span className="block font-semibold text-xl">{cardProps.newCases }</span>
          <span className="block bg-white rounded-full text-orange-500 text-xs font-bold px-3 py-2 leading-none flex items-center">{cardProps.deaths}</span>
        </div>

      </div>
   
   </div>

            
    )
   
}
