import styles from './NavigationBar.module.css';
import Link from 'next/link';

export   function NavigationBar() {
    return (
        <nav className="flex items-center justify-between flex-wrap bg-red-900 p-6">
  <div className="flex items-center flex-shrink-0 text-white mr-6">
  <img
              src="/images/v2.png"
              className={`${styles.headerHomeImage} `}
              alt="logo"
            />
  
    <span className="font-semibold text-xl tracking-tight bg-red-900">Covid-19</span>
  </div>
  <div className="block lg:hidden">
    <button className="flex items-center px-3 py-2 border rounded text-red-200   border-teal-400 hover:text-white hover:border-white">
      <svg className="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
    </button>
  </div>
  <div className="w-full block flex-grow lg:flex lg:items-center lg:w-auto">
    <div className="text-sm lg:flex-grow">
      
      <Link href='/'>
      <a className="block mt-4 lg:inline-block lg:mt-0 text-red-200  text-lg font-bold hover:text-white mr-4">
       Home
      </a>
      </Link>
      <Link href='/Map'>
      <a  className="block mt-4 lg:inline-block lg:mt-0 text-red-200 text-lg font-bold hover:text-white mr-4">
        Map
      </a>
      </Link>
      <Link href='/Sumary'>
      <a  className="block mt-4 lg:inline-block lg:mt-0 text-red-200  text-lg font-bold hover:text-white mr-4">
      The world today!
      </a>
      </Link>
      <Link href='/CovidTestPage'>
      <a  className="block mt-4 lg:inline-block lg:mt-0 text-red-200  text-lg font-bold hover:text-white">
       Do you feel sick?
      </a>
      </Link>
     
    </div>
  </div>
</nav>
    );
}