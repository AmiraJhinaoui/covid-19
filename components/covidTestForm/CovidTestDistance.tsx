export default function CovidTestQuizDistance(){

    return(
        <form className="w-full max-w-lg ">
          <h1 className="text-xl  font-bold text-red-500 mb-4 ">
          Social Distancing
        </h1>
        <div className="flex flex-wrap -mx-3 mb-6">
         
            <label className="block uppercase tracking-wide text-red-200 text-xs font-bold mb-2" >
            Have you been within 6 feet of a person with a lab-confirmed case of COVID-19 for at least 5 minutes, or had direct contact with their mucus or saliva, in the past 14 days?
            </label>
            <select className="block block w-full appearance-none w-full bg-gray-200 border border-gray-200 text-gray-500  py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                <option>YES</option>
                <option>NO</option>
              </select>
            <p className="text-red-500 text-xs italic">Please fill out this field.</p>
          </div>
       
          <div className=" p-10 w-full px-3">
          <button className=" bg-red-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" >
            Next
            </button>
          </div>
        
      </form>
    );
}