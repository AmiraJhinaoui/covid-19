export default function CovidTestSymptoms(){
    return(
        <form className="w-full max-w-lg ">
        <h1 className="text-xl  font-bold text-red-500 mb-3 ">
        Common Symptoms
      </h1>
      <div className="flex flex-wrap -mx-3 mb-6">
        
          <label className="block uppercase tracking-wide text-red-200 text-xs font-bold mb-2" >
          In the last 48 hours, have you had any of the following NEW symptoms?
          </label>
          <div className="mt-2">
                <div>
                <label className="inline-flex items-center">
                    <input type="checkbox" className="form-checkbox h-8 w-8" />
                    <span className="ml-3 text-l text-red-200"> Fever of 100 F (37.8 C) or above</span>
                </label>
                </div>
                <div>
                <label className="inline-flex items-center">
                    <input type="checkbox" className="form-checkbox h-8 w-8" />
                    <span className="ml-3 text-l text-red-200"> Cough</span>
                </label>
                </div>

                 <div>
                <label className="inline-flex items-center">
                    <input type="checkbox" className="form-checkbox h-8 w-8" />
                    <span className="ml-3 text-l text-red-200"> Trouble breathing, shortness of breath or severe wheezing</span>
                </label>
                </div>
                <div>
                <label className="inline-flex items-center">
                    <input type="checkbox" className="form-checkbox h-8 w-8" />
                    <span className="ml-3 text-l text-red-200"> Chills or repeated shaking with chills</span>
                </label>
                </div>

                <div>
                <label className="inline-flex items-center">
                    <input type="checkbox" className="form-checkbox h-8 w-8" />
                    <span className="ml-3 text-l text-red-200"> Muscle aches</span>
                </label>
                </div>

                <div>
                <label className="inline-flex items-center">
                    <input type="checkbox" className="form-checkbox h-8 w-8" />
                    <span className="ml-3 text-l text-red-200"> Sore throat</span>
                </label>
                </div>
                
                <div>
                <label className="inline-flex items-center">
                    <input type="checkbox" className="form-checkbox h-8 w-8" />
                    <span className="ml-3 text-l text-red-200"> Loss of smell or taste, or a change in taste</span>
                </label>
                </div>

                <div>
                <label className="inline-flex items-center">
                    <input type="checkbox" className="form-checkbox h-8 w-8" />
                    <span className="ml-3 text-l text-red-200"> Nausea, vomiting or diarrhea</span>
                </label>
                </div>

                <div>
                <label className="inline-flex items-center">
                    <input type="checkbox" className="form-checkbox h-8 w-8" />
                    <span className="ml-3 text-l text-red-200"> Headache</span>
                </label>
                </div>

                <div>
                <label className="inline-flex items-center">
                    <input type="checkbox" className="form-checkbox h-8 w-8" />
                    <span className="ml-3 text-l text-red-200"> None of all above</span>
                </label>
                </div>
          <p className="text-red-500 text-xs italic">Check all that apply.</p>
    
     </div>
        <div className=" p-5 w-full px-3">
        <button className=" bg-red-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" >
          Next
          </button>
        </div>
        </div>
      
    </form>
    );
}