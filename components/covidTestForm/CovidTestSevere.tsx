export default function CovidTestSevere(){
    return ( 
      <form className="w-full max-w-lg  ">
      <h1 className="text-xl  font-bold text-red-500 mb-3 ">
      Emergency Symptoms
    </h1>
      <div className="flex flex-wrap mx-6 mb-6 ">
      <label className=" uppercase tracking-wide text-red-200 text-xs font-bold mb-2 mx-4" >
      Do you have any of the following possible emergency symptoms?
      </label>

      <div className="mx-4">
            <div>
            <label className="inline-flex items-center">
                <input type="checkbox" className="form-checkbox h-8 w-8" />
                <span className="ml-3 text-l text-red-200"> Struggling to breathe even while inactive or when resting</span>
            </label>
            </div>
            <div>
            <label className="inline-flex items-center">
                <input type="checkbox" className="form-checkbox h-8 w-8" />
                <span className="ml-3 text-l text-red-200"> Feeling about to collapse every time you stand or sit u</span>
            </label>
            </div>

            <div>
            <label className="inline-flex items-center">
                <input type="checkbox" className="form-checkbox h-8 w-8" />
                <span className="ml-3 text-l text-red-200"> None of all above</span>
            </label>
            </div>
      <p className="text-red-500 text-xs italic">Check all that apply.</p>

 </div>
    <div className=" p-5 w-full px-3">
    <button className=" bg-red-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" >
      Next
      </button>
    </div>
    </div>
</form>
    )
}