import {MouseEvent } from "react";


export default function PersonalInfosForm(){
  const handleClick =(e:MouseEvent):void =>{
    e.preventDefault();
    console.log("heyyy you clicked");
  }
    return (
        
        <form className="w-full max-w-lg ">
          <h1 className="text-xl  font-bold text-red-500 mb-4 mx-3 ">
        Please fill your personal informations
        </h1>
        <div className="flex flex-wrap mx-6 mb-6">
          <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
            <label className="block uppercase tracking-wide text-red-200 text-xs font-bold mb-2" >
              Full Name
            </label>
            <input className="appearance-none block w-full bg-gray-200 text-gray-800 border border-gray-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-first-name" type="text" placeholder="Jane"/>
            <p className="text-red-500 text-xs italic">Please fill out this field.</p>
          </div>
          <div className="w-full md:w-1/2 px-3">
            <label className="block uppercase tracking-wide text-red-200 text-xs font-bold mb-2" >
             Gender
            </label>
            <div className="relative">
        <select className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-500 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
          <option>Male</option>
          <option>Female</option>
        </select>
        <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
          <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
        </div>
      </div>
          </div>
        </div>
        <div className="flex flex-wrap mx-6 mb-6 ">
          <div className="w-full px-3">
            <label className="block uppercase tracking-wide text-red-200 text-xs font-bold mb-2" >
              Adress
            </label>
            <input className="appearance-none block w-full bg-gray-200 text-gray-800 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-password" type="text" placeholder="2850 Shadelands Dr
                Suite 101
                Walnut Creek, CA 94598"/>
            <p className="text-red-500 text-xs italic">Please write your adress here</p>
          </div>
        </div>
        <div className="flex flex-wrap mx-6 mb-2">
          <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
            <label className="block uppercase tracking-wide text-red-200 text-xs font-bold mb-2" >
              City
            </label>
            <input className="appearance-none block w-full bg-gray-200 text-gray-800 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-city" type="text" placeholder="Albuquerque"/>
          </div>
          <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
            <label className="block uppercase tracking-wide text-red-200 text-xs font-bold mb-2" >
              State
            </label>
            <div className="relative">
              <select className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-500  py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                <option>New Mexico</option>
                <option>Missouri</option>
                <option>Texas</option>
              </select>
              <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-red-200">
                <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
              </div>
            </div>
          </div>
          <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
            <label className="block uppercase tracking-wide text-red-200 text-xs font-bold mb-2" >
              Zip
            </label>
            <input className="appearance-none block w-full bg-gray-200 text-gray-800 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-zip" type="text" placeholder="90210"/>
          </div>
          <div className=" p-10 w-full px-3">
          <button className=" bg-red-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" onClick={handleClick}>
            Next
            </button>
          </div>
        </div>
      </form>
     
    );

}