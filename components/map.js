import { Component } from 'react';
import ReactMapGL from 'react-map-gl';

class Map extends Component {

  state = {
    viewport: {
      width: '100vw',
      height: '100vh',
      latitude: 36.51,
      longitude: 10.15,
      zoom: 5
    }
  };
 
   geojson = {
    type: 'FeatureCollection',
    features: [{
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [
          -77.032,
           38.913]
      },
      properties: {
        title: 'Mapbox',
        description: 'Washington, D.C.'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [-122.414, 37.776]
      },
      properties: {
        title: 'Mapbox',
        description: 'San Francisco, California'
      }
    },
    ]
  };

  render() {
    return (
       
      <ReactMapGL
        mapStyle="mapbox://styles/mapbox/dark-v9"
        mapboxApiAccessToken="pk.eyJ1IjoiYW1pcmFqaCIsImEiOiJja2I5aTd4YmswZWhiMnJwaXM3bHU4ZnYwIn0.-8zxOJkvGAsRGaRTBy8mlg"
        onViewportChange={(viewport) => this.setState({ viewport })}
        {...this.state.viewport}
      />

    );
  }
}

export default Map;

