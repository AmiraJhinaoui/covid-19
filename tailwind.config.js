module.exports = {
    theme: {
        theme: {
            customForms: theme => ({
              default: {
                checkbox: {
                  width: theme('spacing.6'),
                  height: theme('spacing.6'),
                  iconColor: theme('colors.red.700'),
                },
              },
             
            }),
            
          },
     
    purge: [
        './pages/**/*.tsx',
        './components/**/*.tsx'
    ],
    plugins: [
        require('@tailwindcss/custom-forms'),
      ],


      
     
  },
  extend: {
    colors: {
      blood: '#742A2A',
      rosewood:'#600000',
      red: '#de3618',
      grayishRed:'#F6EBEB',
      cookie: '#f1dada',
      oracle:'#006060',
      blue: '#007ace',
      strongBlue: '#2A4774',
      darkGreen: '#2A7447',
      greeny: '#74742a',
      
     
    }
  }}

 