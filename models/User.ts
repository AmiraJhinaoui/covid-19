export interface User{
    fullName:String;
    gender: string;
    adress: string;
    city: string;
    state: string;
    zip:number;  
}