export interface IContinentCasesInterface {
    updated: number
    cases: number
    todayCases: number
    deaths: number
    todayDeaths: number,
    recovered: number
    active: number,
    critical: number,
    continent:String
}
export interface ICountryCases{
    Country: string,
    CountryCode: string,
    Lat: number,
    Lon: number,
    Confirmed: number,
    Deaths: number,
    Recovered: number,
    Active: number,
    Date: Date,
    LocationID:number
}
export interface IGlobalCases {   
    NewConfirmed: number,
    TotalConfirmed: number,
    NewDeaths: number,
    TotalDeaths: number,
    NewRecovered: number,
    TotalRecovered: number,    
}
export interface ISumaryCases {
    global: IGlobalCases,
    countries: ICountryCases[],
}